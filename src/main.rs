use actix_web::client::Client;
use serde_json::json;
use serde::{Deserialize, Serialize};
use chrono::{NaiveDateTime, Datelike, Duration};
use std::u32;
use futures::future::join_all;
use std::rc::Rc;
use std::collections::HashMap;
use sorted_list::SortedList;
use std::fs::File;
use std::io::prelude::*;
use yaml_rust::yaml::{Yaml};
use yaml_rust::YamlLoader;
use num::FromPrimitive;
use std::ops::Add;
use std::ops::Sub;
use lazy_static::lazy_static;
use std::fs::OpenOptions;
use std::io::Write;
use std::clone::Clone;

extern crate rpassword;

#[derive(Serialize, Deserialize, Clone)]
struct JiraIssue {
    expand: String,
    id: String,
    #[serde(rename = "self")] 
    jira_self: String,
    key: String,
} 


#[derive(Serialize, Deserialize)]
struct JiraAntwort {
    expand: String,
    #[serde(rename = "startAt")] 
    start_at: u32,
    #[serde(rename = "maxResults")] 
    max_results: u32,
    total:u32, 
    issues: Vec<JiraIssue>,
}

#[derive(Serialize, Deserialize, Clone)]
struct JiraAuthor {
    #[serde(rename = "self")] 
    jira_self: String,
    key: String,
    name: String,
} 


#[derive(Serialize, Deserialize, Clone)]
struct JiraWorklog {
    #[serde(rename = "self")] 
    jira_self: String,
    started: String,
    #[serde(rename = "timeSpentSeconds")] 
    time_spent_seconds: u32,
    #[serde(rename = "issueId")] 
    issue_id: String,
    author: JiraAuthor,
} 

#[derive(Serialize, Deserialize)]
struct JiraAntwortWorklog {
    #[serde(rename = "startAt")] 
    start_at: u32,
    #[serde(rename = "maxResults")] 
    max_results: u32,
    total:u32, 
    worklogs: Vec<JiraWorklog>,
}

struct Worklog {
    jira_worklog: JiraWorklog,
    jira_issue: Rc<JiraIssue>,
}

struct Wochenergebnis{
    table: String,
    duration_differenz_gesamt: Duration,
}

impl PartialEq for Worklog {
    fn eq(&self, other: &Self) -> bool {
        return self.jira_worklog.jira_self == other.jira_worklog.jira_self;
    }
}

lazy_static! {

    static ref CONFIG: Yaml = {
        let conf: Yaml = load_config ();
        conf
    };

    static ref  VERSIONEN: Vec<String> = {
        let mut temp_versionen: Vec<String> = Vec::new();
        temp_versionen.push("".to_string());
        for v in CONFIG["versionen"].as_vec().unwrap() {
            temp_versionen.push(v.as_str().unwrap().to_string());
        }
        temp_versionen
    };
}


#[actix_rt::main]
async fn main() {
    let login_user = CONFIG["login_user"].as_str().unwrap().to_string();
    let user = CONFIG["user"].as_str().unwrap().to_string();
    let kw = CONFIG["kw_off_interest"].as_str().unwrap().to_string();
    let kw_off_interest = i32::from_str_radix(&kw, 10).unwrap();
    let passwort = rpassword::prompt_password_stdout("Passwort:").unwrap();
    let mut duration_differenz_gesamt = Duration::nanoseconds(0);


    let worklogs: Vec<Worklog> = get_worklog_for_week(-1, &login_user , &passwort).await;

    let mut table = String::new();
    for week in 1..kw_off_interest {
        let user_week_day_worklogs = create_user_week_day_struct(&worklogs, &user, week);
        let table_duration = create_table_comment(&user_week_day_worklogs, &user, week, duration_differenz_gesamt);
        println!("{}",table_duration.table);
        duration_differenz_gesamt = table_duration.duration_differenz_gesamt;
        table += &table_duration.table;
    } 
    let num_worklogs = worklogs.len();
    println!("Gefunden Worklogs gesamt: {}", num_worklogs);	
    write_to_file(&table);
}

fn write_to_file(str: &String) {
    let target_filename = CONFIG["target_filename"].as_str().unwrap().to_string();
    let mut file = OpenOptions::new()
        .append(true)
        .create(true)
        .open(target_filename).expect(
        "cannot open file");
    // let mut file = File::open(target_filename).unwrap();
    file.write_all(str.as_bytes()).expect("Dumm gelaufen");
    file.flush().expect("Dumm gelaufen 2");
}

async fn get_worklog_for_week(week_of_year: i32, login_user:&String, passwort:&String) -> Vec<Worklog> {
    let mut alle_worklogs:  Vec<Worklog> = Vec::new();
    let client = Client::default();
    let value = json!({
        "jql":"worklogDate>=-300d",
        "startAt":0,
        "maxResults":10000,
        "fields":["key"]
    });
    // let b = Body::from(value);
    let jira_address = CONFIG["jira_address"].as_str().unwrap().to_string();
    let url = format!(r"{}/rest/api/2/search", jira_address);
    let timeout = Duration::minutes(3).to_std().unwrap();
    let response2 = client.post(url)
        .basic_auth(login_user, Some(&passwort) )
        .header("Accept-Encoding", "gzip,deflate")
        .header(http::header::CONTENT_TYPE, "application/json")
        .timeout(timeout)
        .send_body(value)
        .await;

    if response2.is_ok() {
        let mut r = response2.unwrap();
        let mb = r.body().limit(5242880).await.unwrap();
        let lines = std::str::from_utf8(&mb).unwrap();
        let antwort: JiraAntwort = serde_json::from_str(lines).unwrap();
        let mut futures = vec![];
        for issue in antwort.issues.iter() {
            if issue.key.starts_with("AMS") || issue.key.starts_with("BEL")  {
                // println!("Key: {}", issus.key);
                futures.push(get_worklog_from_issue( issue, week_of_year, &login_user, &passwort));
                
            }
        }
        let results = join_all(futures).await;
        for mut w in results {
            alle_worklogs.append(&mut w);
        }
    }
    return alle_worklogs;
}

// {{KW }}
// ||Tag||Anfang||Ende||Pause (Minuten)||Gesamt||
// | | | | | |
// | | | | | |
// | | | | | |
// | | | | | |
// |*KW *| | | | |
fn create_table_comment(user_week_day_worklogs: &HashMap<String, HashMap<i32, HashMap<chrono::Weekday, SortedList<NaiveDateTime, Worklog>>>>, user: &String, week_of_year: i32, duration_differenz_vorwoche: Duration) -> Wochenergebnis {
    let mut table: String = String::new();
    let worktime_per_day_minutes_t1 = CONFIG["expected_dayly_worktime_minutes"].as_str().unwrap().to_string();
    let worktime_per_day_minutes_t2 = i64::from_str_radix(&worktime_per_day_minutes_t1, 10).unwrap();
    let duration_worktime_per_day_minutes: Duration = Duration::minutes(worktime_per_day_minutes_t2);

    table += "\n\n";
    table += &CONFIG["versionen"][0].as_str().unwrap().to_string();
    table += &format!("\n\n{{{{{}}}}}\n||Tag||Anfang||Ende||Pause ||Gesamt||\n", week_of_year);
    let week_day_worklogs = user_week_day_worklogs.get(user).unwrap();
    let day_worklogs = week_day_worklogs.get(&week_of_year);
    let mut duration_work_week: Duration = Duration::seconds(0);
    let mut duration_rest_week: Duration = Duration::seconds(0);
    let mut duration_expected_work_week: Duration = Duration::seconds(0);
    let mut duration_differenz_gesamt: Duration = Duration::nanoseconds(0);
    if day_worklogs.is_some() {
        for n in 0..6 {
            let worklogs = day_worklogs.unwrap().get(&chrono::Weekday::from_u64(n).unwrap()); 
            let mut first_date: Option<NaiveDateTime> = None;
            let mut last_date: Option<NaiveDateTime> = None;
            let mut number_work_seconds = 0;
            let mut last_seconds = 0;
            if worklogs.is_some() {
                for worklog in worklogs.unwrap().values() {
                    let datestr= worklog.jira_worklog.started.to_string();
                    let date = NaiveDateTime::parse_from_str(&datestr, "%Y-%m-%dT%H:%M:%S.%f%z").unwrap();
                    if first_date.is_none() {
                        first_date = Some(date);
                    } 
                    last_date = Some(date);
                    last_seconds = worklog.jira_worklog.time_spent_seconds;
                    number_work_seconds += worklog.jira_worklog.time_spent_seconds;
                    // println!("{} {} {} Seconds: {}", n,datestr, worklog.jira_issue.key, worklog.jira_worklog.time_spent_seconds);
                }
            }
            if first_date.is_some() {
                let last_worklog_duration = Duration::seconds(last_seconds as i64); 
                let first_date = first_date.unwrap();
                let last_date = last_date.unwrap_or(first_date);
                let t = last_date;
                let last_date_time = t.add(last_worklog_duration);
                let durtation_start_ende = last_date_time - first_date;
                let duration_work_day = Duration::seconds(number_work_seconds as i64);
                let duration_rest_day = durtation_start_ende - duration_work_day;
                duration_rest_week = duration_rest_week.add(duration_rest_day);
                duration_work_week = duration_work_week.add(duration_work_day);
                let line = format!("|{}|{}|{}|{}|{}|\n", first_date.format("%F").to_string(), first_date.format("%R").to_string(), last_date_time.format("%R").to_string(), create_string(&duration_rest_day), create_string(&duration_work_day));
                table += &line;
                duration_expected_work_week = duration_expected_work_week.add(duration_worktime_per_day_minutes);
            }
        }
        let duration_differenz = duration_work_week.sub(duration_expected_work_week);
        duration_differenz_gesamt = duration_differenz_vorwoche.add(duration_differenz);
        let mut line = format!("| KW {} | {} | {} | *{}* | *{}* |\n", week_of_year, "", "", create_string(&duration_rest_week), create_string(&duration_work_week));
        line += &format!("| {} | {} | {} | *{}* | *{}* |\n", "Erwartete Wochenarbeitszeit", "", "", "", create_string(&duration_expected_work_week));
        line += &format!("| {} | {} | {} | *{}* | *{}* |\n", "Differenz", "", "", "", create_string(&duration_differenz));
        line += &format!("| {} | {} | {} | *{}* | *{}* |\n", "Differenz Vorwoche", "", "", "", create_string(&duration_differenz_vorwoche));
        line += &format!("| {} | {} | {} | *{}* | *{}* |\n", "Differenz Gesamt", "", "", "", create_string(&duration_differenz_gesamt));

        table += &line;
    }
    let wochenergebnis = Wochenergebnis {
        table: table,
        duration_differenz_gesamt: duration_differenz_gesamt,
    }; 
    return wochenergebnis;
}

fn create_string(duration: &Duration) -> String {
    let hour: i64 = duration.num_hours();
    let min: i64 = duration.num_minutes() % 60;

    let str = format!("{:0>2}h {:0>2}m", hour, min);
    return str;
}


fn create_user_week_day_struct(worklogs: &Vec<Worklog>, user: &String, week_of_year: i32) -> HashMap<String, HashMap<i32, HashMap<chrono::Weekday, SortedList<NaiveDateTime, Worklog>>>> {
    let mut user_week_day_worklogs: HashMap<String, HashMap<i32, HashMap<chrono::Weekday, SortedList<NaiveDateTime, Worklog>>>> = HashMap::new();
    let mut week_day_worklogs: HashMap<i32, HashMap<chrono::Weekday, SortedList<NaiveDateTime, Worklog>>>;
    let mut day_worklogs: HashMap<chrono::Weekday, SortedList<NaiveDateTime, Worklog>>;
    let year = CONFIG["year_of_interest"].as_str().unwrap().to_string();
    let year_off_interest = i32::from_str_radix(&year, 10).unwrap();
    match user_week_day_worklogs.remove(user) {
        Some(x) => week_day_worklogs = x,
        None => week_day_worklogs = HashMap::new(),
    }
        
    match week_day_worklogs.remove(&week_of_year) {
        Some(x) => day_worklogs = x,
        None => day_worklogs = HashMap::new(),
    }

    for w in worklogs {
        let date = NaiveDateTime::parse_from_str(&w.jira_worklog.started, "%Y-%m-%dT%H:%M:%S.%f%z").unwrap();
        let week: u32 = date.iso_week().week();
        let year: i32 = date.year();  
        if (week as i32 == week_of_year) && (&w.jira_worklog.author.name == user) && (year == year_off_interest) {
            let mut worklogs: SortedList<NaiveDateTime, Worklog>;
            let day: chrono::Weekday = date.weekday(); 
            match day_worklogs.remove(&day){
                Some(x) => worklogs = x,
                None => worklogs = SortedList::new(),
            }
            let w_temp:Worklog = Worklog {
                jira_worklog: w.jira_worklog.clone(),
                jira_issue: w.jira_issue.clone(),
            };
            worklogs.insert(date, w_temp);
            day_worklogs.insert(day, worklogs);
        }
    }
    week_day_worklogs.insert(week_of_year, day_worklogs);
    user_week_day_worklogs.insert(user.to_string(), week_day_worklogs);
    return user_week_day_worklogs;
}


async fn get_worklog_from_issue(issue: &JiraIssue, week_of_year: i32, login_user:&String, passwort:&String) -> Vec<Worklog> {
    let mut alle_worklogs:  Vec<Worklog> = Vec::new();
    let jira_address = CONFIG["jira_address"].as_str().unwrap().to_string();
    let url = format!(r"{}/rest/api/2/issue/{}/worklog", jira_address,  &issue.key);
	let mut response_ok = false;
	let mut countdown = 100;
	
	while !response_ok || countdown == 0 { // Leider ist Jira nicht besonders zuverlässig. Eventuel müssen wir eine Anfrage öfters stellen.
	    let client = Client::default();
	    let box_issue = Rc::new((*issue).clone());
	    let timeout = std::time::Duration::new(180, 0);
	    let response1 = client.get(url.clone())
	        .basic_auth(login_user, Some(&passwort) )
	        .header("User-Agent", "actix-web/3.0")
	        .timeout(timeout)
	        .send()     // <- Send request
	        .await;
	
	    if response1.is_ok() {
			response_ok = true;
	        let mut r = response1.unwrap();
	        // println!("{}" , issue.key);
	        let mb = r.body().limit(5242880).await.unwrap();
	        let lines = std::str::from_utf8(&mb).unwrap();
	        let mut antwort: JiraAntwortWorklog = serde_json::from_str(lines).unwrap();
			
			
			
	        while antwort.worklogs.len() > 0 {
	            let worklog = antwort.worklogs.pop().unwrap();
	            let week: u32 = NaiveDateTime::parse_from_str(&worklog.started, "%Y-%m-%dT%H:%M:%S.%f%z").unwrap().iso_week().week();               
	            if week_of_year >= 0 {
	                if week as i32 == week_of_year {
	                    let w: Worklog = Worklog {jira_worklog: worklog, jira_issue: Rc::clone(&box_issue)};
	                    alle_worklogs.push(w);
	                    // time_spent_seconds_totoal += worklog.time_spent_seconds;
	                    // println!("   Issue ID: {} Time: {} Week: {}  ---> Started: {} ", worklog.issue_id, worklog.time_spent_seconds, week, worklog.started);
	                }
	            } else {
	                let w: Worklog = Worklog {jira_worklog: worklog, jira_issue: Rc::clone(&box_issue)};
	                alle_worklogs.push(w);
	                }
	            
	        }
//			let t = format!("Issue Key: {} Antwort startAt: {} Antwort maxResults: {} Anwort total: {}\n", &issue.key, antwort.start_at, antwort.max_results, antwort.total);
//			write_to_file2(&t);
	    } else {
			countdown -= 1;
			// println!("Kein Ergebnis für {}", &issue.key);
		}
	}
    return alle_worklogs;
}

fn load_config () -> Yaml  {
    let file = "config_jira_worklog_auslesen.yaml";
    let mut file = File::open(file).expect("Unable to open file");
    let mut contents = String::new();
    file.read_to_string(&mut contents).expect("Unable to read file");
    let mut data = YamlLoader::load_from_str(&contents).unwrap();
    return data.remove(0);
}